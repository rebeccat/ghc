.. _release-9-8-1:

Version 9.8.1
=============

Language
~~~~~~~~

Compiler
~~~~~~~~

- Added a new warning :ghc-flag:`-Wterm-variable-capture` that helps to make code compatible with
  the future extension ``RequiredTypeArguments``.

- Rewrite rules now support a limited form of higher order matching when a
  pattern variable is applied to distinct locally bound variables. For example: ::

      forall f. foo (\x -> f x)

  Now matches: ::

      foo (\x -> x*2 + x)

GHCi
~~~~


Runtime system
~~~~~~~~~~~~~~

- On POSIX systems that support timerfd, RTS shutdown no longer has to wait for
  the next RTS 'tick' to occur before continuing the shutdown process. See #22692.

``base`` library
~~~~~~~~~~~~~~~~


``ghc-prim`` library
~~~~~~~~~~~~~~~~~~~~

``ghc`` library
~~~~~~~~~~~~~~~

``ghc-heap`` library
~~~~~~~~~~~~~~~~~~~~


Included libraries
------------------

The package database provided with this distribution also contains a number of
packages other than GHC itself. See the changelogs provided with these packages
for further change information.

.. ghc-package-list::

    libraries/array/array.cabal:             Dependency of ``ghc`` library
    libraries/base/base.cabal:               Core library
    libraries/binary/binary.cabal:           Dependency of ``ghc`` library
    libraries/bytestring/bytestring.cabal:   Dependency of ``ghc`` library
    libraries/Cabal/Cabal/Cabal.cabal:       Dependency of ``ghc-pkg`` utility
    libraries/Cabal/Cabal-syntax/Cabal-syntax.cabal:  Dependency of ``ghc-pkg`` utility
    libraries/containers/containers/containers.cabal: Dependency of ``ghc`` library
    libraries/deepseq/deepseq.cabal:         Dependency of ``ghc`` library
    libraries/directory/directory.cabal:     Dependency of ``ghc`` library
    libraries/exceptions/exceptions.cabal:   Dependency of ``ghc`` and ``haskeline`` library
    libraries/filepath/filepath.cabal:       Dependency of ``ghc`` library
    compiler/ghc.cabal:                      The compiler itself
    libraries/ghci/ghci.cabal:               The REPL interface
    libraries/ghc-boot/ghc-boot.cabal:       Internal compiler library
    libraries/ghc-boot-th/ghc-boot-th.cabal: Internal compiler library
    libraries/ghc-compact/ghc-compact.cabal: Core library
    libraries/ghc-heap/ghc-heap.cabal:       GHC heap-walking library
    libraries/ghc-prim/ghc-prim.cabal:       Core library
    libraries/haskeline/haskeline.cabal:     Dependency of ``ghci`` executable
    libraries/hpc/hpc.cabal:                 Dependency of ``hpc`` executable
    libraries/integer-gmp/integer-gmp.cabal: Core library
    libraries/libiserv/libiserv.cabal:       Internal compiler library
    libraries/mtl/mtl.cabal:                 Dependency of ``Cabal`` library
    libraries/parsec/parsec.cabal:           Dependency of ``Cabal`` library
    libraries/pretty/pretty.cabal:           Dependency of ``ghc`` library
    libraries/process/process.cabal:         Dependency of ``ghc`` library
    libraries/stm/stm.cabal:                 Dependency of ``haskeline`` library
    libraries/template-haskell/template-haskell.cabal: Core library
    libraries/terminfo/terminfo.cabal:       Dependency of ``haskeline`` library
    libraries/text/text.cabal:               Dependency of ``Cabal`` library
    libraries/time/time.cabal:               Dependency of ``ghc`` library
    libraries/transformers/transformers.cabal: Dependency of ``ghc`` library
    libraries/unix/unix.cabal:               Dependency of ``ghc`` library
    libraries/Win32/Win32.cabal:             Dependency of ``ghc`` library
    libraries/xhtml/xhtml.cabal:             Dependency of ``haddock`` executable
